const fs = require('fs');
const path = "./messages";


module.exports = {
  add(item) {
    this.save(item.datetime, item);
    return item;
  },

  save(date, file) {
    fs.writeFileSync(`${path}/${date}.txt`, JSON.stringify(file));
  }
};

