const express = require('express');
const router = express.Router();
const fileDb = require('../fileDb');
const fs = require('fs');

const path = "./messages";


router.get('/', (req, res) => {
  fs.readdir(path, (err, files) => {
    let messages = [];
    files.forEach(file => {
      messages.push(JSON.parse(fs.readFileSync(`${path}/${file}`, 'utf-8')));
    });
    if (messages.length > 5) {
      return res.send(messages.slice(messages.length - 5))
    }
    return res.send(messages);
  })
});


router.post('/', (req, res) => {
  const date = new Date().toISOString();
  const done = fileDb.add({
    message: req.body.message,
    datetime: date,
  });
  res.send(done);
})


module.exports = router;